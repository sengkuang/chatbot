import sys
import os.path
import json
import time
# print(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir,"","p512_h1024_x2")))
# sys.exit()
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir,"","p512_h1024_x2")))
from execute import execute
import s2s_reader
import re
from nltk.stem.snowball import EnglishStemmer

file=open("/media/skyap/Data/Cornell Chatbot/files/dialogs_in_token","r")

reader=s2s_reader.reader("",1,[5,10,15])
ex = execute()

def print_sentence(index_list):
    return " ".join([reader.id_dict[index] for index in index_list])



print(chr(27) + "[2J")
for line in file:
	pair=json.loads(line)
	question=print_sentence(pair[0])
	answer=ex.decode_line(question)
	print("A: ",question)
	print("B: ",answer)
	print("-"*50)
	print("\n")
	time.sleep(2)
