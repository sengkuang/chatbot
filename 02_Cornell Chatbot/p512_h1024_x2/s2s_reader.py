import json
import numpy as np
import shelve

class reader:
    def __init__(self,folder_name,batch_size,bucket_option):
        self.epoch=1
        self.batch_size=batch_size
        self.folder_name=folder_name
        self.output=[]
        self.bucket_option=bucket_option
        self.buckets=self.create_bucket()
        self.clean_stock=False
        self.bucket_dict=self.build_bucket_dict()
        self.bucket_list=[]
        for _ in range(len(self.buckets)):
            self.bucket_list.append([])
            
        self.file=open("/media/skyap/Data/Cornell Chatbot/files/dialogs_in_token","r")
        self.token_count_dict=shelve.open("/media/skyap/Data/Cornell Chatbot/files/token_count_dict.db","r")
        
        self.id_dict={}
        for key in self.token_count_dict:
            self.id_dict[self.token_count_dict[key]]=key
        
    def create_bucket(self):
        buckets=[]
        for i,j in zip(self.bucket_option,self.bucket_option[1:]):
            buckets.append((i,j))
        return buckets
    
    def build_bucket_dict(self):
        bucket_dict={}
        index=0
        for i in range(self.bucket_option[-1]+1):
            lo,hi=self.buckets[index]
            if i<=lo:
                bucket_dict[i]=index
                continue
            if i>lo and index<len(self.buckets)-1:
                index+=1
                bucket_dict[i]=index
                continue
        return bucket_dict
    
    def check_bucket(self,pair):
        # choose bucket, remember [(input_size,output_size)]
        # if input_size is 5 and output_size is 11, choose busket 1
        # if input_size is 11 and output_size is 2, choose busket 1
        input_len=len(pair[0])
        output_len=len(pair[1])
        if input_len in self.bucket_dict and output_len in self.bucket_dict:
            input_size=self.bucket_dict[len(pair[0])]
            output_size=self.bucket_dict[len(pair[1])]
            return max(input_size,output_size)
        else:
            return -1

    def fill_bucket(self):
        while True:
            line=self.file.readline()
            if not line:
                break
            pair=json.loads(line)
            index=self.check_bucket(pair)
            if index==-1:
                continue
            self.bucket_list[index].append(pair)
            if len(self.bucket_list[index])==self.batch_size:
                return index
        return -1        
        
    def next_batch(self):
        if not self.clean_stock:
            index=self.fill_bucket()
            if index>=0:
                output=self.bucket_list[index]
                self.bucket_list[index]=[]
                return output,index
            else:
                self.clean_stock=True
                for i in range(len(self.bucket_list)):
                    if len(self.bucket_list[i])>0:
                        output=self.bucket_list[i]
                        self.bucket_list[i]=[]
                        return output,i
        else:
            for i in range(len(self.bucket_list)):
                if len(self.bucket_list[i])>0:
                    output=self.bucket_list[i]
                    self.bucket_list[i]=[]
                    return output,i
                self.clean_stock=False
                self.reset()
                return self.next_batch()                
        
    def reset(self):
        self.epoch+=1
        self.file.close()
        self.file=open("/media/skyap/Data/Cornell Chatbot/files/dialogs_in_token","r")
        
    def data_processing(self,data,index):
        enc_len,dec_len=self.buckets[index]
        
        enc_inp=np.zeros((enc_len,self.batch_size))
        dec_inp=np.zeros((dec_len,self.batch_size))
        dec_tar=np.zeros((dec_len,self.batch_size))
        
        for i in range(len(data)):
            pair=data[i]
            enc_inp[enc_len-len(pair[0]):enc_len,i]=pair[0][::-1]
            dec_inp[1:len(pair[1])+1,i]=pair[1]
            dec_tar[0:len(pair[1]),i]=pair[1]
            dec_inp[0,i]=2
            dec_tar[len(pair[1]),i]=3
            
        return enc_inp,dec_inp,dec_tar    

if __name__=="__main__":
    a=reader('',10,[5,10,15,20,25,31])
    data,index=a.next_batch()
    print(data)
    print(index)
    enc_inp,dec_inp,dec_tar=a.data_processing(data,index)
    np.set_printoptions(suppress=True)
    print("enc_inp",enc_inp)
    print("dec_inp",dec_inp)
    print("dec_tar",dec_tar)
    