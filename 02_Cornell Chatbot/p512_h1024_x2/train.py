import tensorflow as tf
import json
import s2s_reader
import os
import time

load_model=True

root_folder="/media/skyap/Data/Cornell Chatbot"

batch_size=128
# bucket_option=[5,10,15,20,25,31]
bucket_option=[5,10,15]
reader=s2s_reader.reader('',batch_size,bucket_option)
vocab_size=len(reader.token_count_dict)

hidden_size=1024
projection_size=512
embedding_size=300
num_layers=2
output_size=projection_size

truncated_std=0.1
keep_prob=0.95
max_epoch=300
norm_clip=5

adam_learning_rate=0.001


model_name="p"+str(projection_size)+"_h"+str(hidden_size)+"_x"+str(num_layers)+"_"+time.strftime("%Y%m%d-%H%M%S")
save_path=root_folder+"/model/"+model_name
print("save_path: ",save_path)

# tf.set_random_seed(1)
tf.reset_default_graph()
sess=tf.InteractiveSession()

enc_inputs=tf.placeholder(tf.int32,shape=(None,batch_size),name="enc_inputs")
targets=tf.placeholder(tf.int32,shape=(None,batch_size),name="targets")
dec_inputs=tf.placeholder(tf.int32,shape=(None,batch_size),name="dec_inputs")

emb_weights=tf.Variable(tf.truncated_normal([vocab_size,embedding_size],stddev=truncated_std),name="emb_weights")
enc_inputs_emb=tf.nn.embedding_lookup(emb_weights,enc_inputs,name="enc_inputs_emb")
dec_inputs_emb=tf.nn.embedding_lookup(emb_weights,dec_inputs,name="dec_inputs_emb")

def getLSTM():
    cell_list=[]
    for i in range(num_layers):
        single_cell=tf.contrib.rnn.LSTMCell(
                                        num_units=hidden_size,
                                        num_proj=projection_size,
                                        state_is_tuple=True
                                        )
        if i <num_layers-1 or num_layers ==1:
            single_cell=tf.contrib.rnn.DropoutWrapper(cell=single_cell,
                                             output_keep_prob=keep_prob)
        cell_list.append(single_cell)
    return tf.contrib.rnn.MultiRNNCell(cells=cell_list,state_is_tuple=True)

_,enc_states=tf.nn.dynamic_rnn(cell=getLSTM(),
                               inputs=enc_inputs_emb,
                               dtype=tf.float32,
                               time_major=True,
                               scope='encoder')
dec_outputs,dec_states=tf.nn.dynamic_rnn(cell=getLSTM(),
                                        inputs=dec_inputs_emb,
                                        initial_state=enc_states,
                                        dtype=tf.float32,
                                        time_major=True,
                                        scope='decoder')

project_w=tf.Variable(tf.truncated_normal(shape=[output_size,embedding_size],
                                          stddev=truncated_std),name="project_w")
project_b=tf.Variable(tf.constant(shape=[embedding_size],value=0.1),name="project_b")
softmax_w=tf.Variable(tf.truncated_normal(shape=[embedding_size,vocab_size],
                                          stddev=truncated_std),name="softmax_w")
softmax_b=tf.Variable(tf.constant(shape=[vocab_size],value=0.1),name="softmax_b")

dec_outputs=tf.reshape(dec_outputs,[-1,output_size],name="dec_outputs")
dec_proj=tf.matmul(dec_outputs,project_w)+project_b
logits=tf.nn.log_softmax(tf.matmul(dec_proj,softmax_w)+softmax_b,name="logits")

flat_targets=tf.reshape(targets,[-1])
total_loss=tf.nn.sparse_softmax_cross_entropy_with_logits(logits=logits,labels=flat_targets)
avg_loss=tf.reduce_mean(total_loss)

optimizer=tf.train.AdamOptimizer(adam_learning_rate)

gvs=optimizer.compute_gradients(avg_loss)
capped_gvs=[(tf.clip_by_norm(grad,norm_clip),var)for grad,var in gvs]
train_op=optimizer.apply_gradients(capped_gvs)

saver=tf.train.Saver()
    
if load_model:
    os.mkdir(save_path)
    saver.restore(sess,root_folder+"/model/p512_h1024_x2_20171210-072215/model.ckpt")
    with open(root_folder+"/model/p512_h1024_x2_20171210-072215/summary.json")as json_data:
        losses=json.load(json_data)
        reader.epoch=len(losses)+1
    print("Model restored")
else:
    os.mkdir(save_path)
    sess.run(tf.global_variables_initializer())
    losses=[]

    
def update_summary(save_path,losses):
    summary_location=save_path+"/summary.json"
    if os.path.exists(summary_location):
        os.remove(summary_location)
    with open(summary_location,'w') as outfile:
        json.dump(losses,outfile)
        
count=0
epoch_loss=0
epoch_count=0

while True:
    curr_epoch=reader.epoch
    data,index=reader.next_batch()
    
    enc_inp,dec_inp,dec_tar=reader.data_processing(data,index)
    
    if reader.epoch!=curr_epoch:
        print("\n----------end of epoch:" + str(reader.epoch-1) + "----------")
        print("    avg loss: " + str(epoch_loss/epoch_count))
        print("\n")
        
        losses.append(epoch_loss/epoch_count)
        
        epoch_loss=0
        epoch_count=0
        
        update_summary(save_path,losses)
        saver.save(sess,save_path+"/model.ckpt")
        print("Model saved")
        
        if reader.epoch==(max_epoch+1):
            break
    feed_dict={enc_inputs:enc_inp,dec_inputs:dec_inp,targets:dec_tar}
    _,loss_t=sess.run([train_op,avg_loss],feed_dict)
    epoch_loss+=loss_t
    
    count+=1
    epoch_count+=1
    
    if count%10==0:
        print(str(loss_t) + " @ epoch: " + str(reader.epoch) + " count: "+ str(epoch_count * batch_size))



sess.close()
