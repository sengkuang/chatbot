import tensorflow as tf
import numpy as np
import s2s_reader
import sys
import re
from nltk.stem.snowball import EnglishStemmer



class execute():
    def __init__(self):
        self.root_folder="/media/skyap/Data/Cornell Chatbot"
        #regular expression for parsing user input
        self.expression = r"[a-zA-Z0-9']+"
        self.english=EnglishStemmer()

        #batch size for testing
        self.batch_size = 1

        #data params
        #bucket_option = [i for i in xrange(1, 20+1)]
        self.bucket_option = [5,10,15]

        self.reader = s2s_reader.reader("",self.batch_size,self.bucket_option)
        self.vocab_size = len(self.reader.token_count_dict)

        # if load_model = true, then we need to define the same parameter in the saved_model inorder to load it 
        self.hidden_size = 1024
        self.projection_size = 512
        self.embedding_size = 300
        self.num_layers = 2
        self.output_size = self.projection_size

        self.keep_prob=0.95

        #model name & save path
        # model_name="p"+str(projection_size)+"_h"+str(hidden_size)+"_x"+str(num_layers)
        self.save_path= self.root_folder+"/model/"+"p512_h1024_x2_20171210-072215"

        #prediction params
        self.beam_size = 10
        self.top_k = 10
        self.max_sequence_len = 20
        self.load_graph()



    def load_graph(self):
        #---------------------------------model definition------------------------------------------
        # tf.set_random_seed(1)
        tf.reset_default_graph()
        self.sess = tf.InteractiveSession()

        #placeholder
        self.enc_inputs = tf.placeholder(tf.int32, shape=(None, self.batch_size), name="enc_inputs")
        self.dec_inputs = tf.placeholder(tf.int32, shape=(None, self.batch_size), name="dec_inputs")

        #input embedding layers
        emb_weights = tf.Variable(tf.random_normal([self.vocab_size, self.embedding_size]), name="emb_weights")
        enc_inputs_emb = tf.nn.embedding_lookup(emb_weights, self.enc_inputs, name="enc_inputs_emb")
        dec_inputs_emb = tf.nn.embedding_lookup(emb_weights, self.dec_inputs, name="dec_inputs_emb")

        #cell definiton
        def getLSTM():
            cell_list=[]
            for i in range(self.num_layers):
                single_cell=tf.contrib.rnn.LSTMCell(
                                                num_units=self.hidden_size,
                                                num_proj=self.projection_size,
                                                state_is_tuple=True
                                                )
                # if i <num_layers-1 or num_layers ==1:
                #     single_cell=tf.contrib.rnn.DropoutWrapper(cell=single_cell,
                #                                      output_keep_prob=keep_prob)
                cell_list.append(single_cell)
            return tf.contrib.rnn.MultiRNNCell(cells=cell_list,state_is_tuple=True)

        #encoder & decoder defintion
        _, self.enc_states = tf.nn.dynamic_rnn(cell = getLSTM(), 
            inputs = enc_inputs_emb, 
            dtype = tf.float32, 
            time_major = True, 
            scope="encoder")

        dec_outputs, self.dec_states = tf.nn.dynamic_rnn(cell = getLSTM(), 
            inputs = dec_inputs_emb, 
            initial_state = self.enc_states, 
            dtype = tf.float32, 
            time_major = True, 
            scope="decoder")

        #output layers
        project_w = tf.Variable(tf.truncated_normal([self.output_size, self.embedding_size], stddev=0.1), name="project_w")
        project_b = tf.Variable(tf.constant(0.1, shape=[self.embedding_size]), name="project_b")
        softmax_w = tf.Variable(tf.truncated_normal([self.embedding_size, self.vocab_size], stddev=0.1), name="softmax_w")
        softmax_b = tf.Variable(tf.constant(0.1, shape=[self.vocab_size]), name="softmax_b")

        dec_outputs = tf.reshape(dec_outputs, [-1, self.output_size])
        dec_proj = tf.matmul(dec_outputs, project_w) + project_b
        logits = tf.nn.log_softmax(tf.matmul(dec_proj, softmax_w) + softmax_b)

        #prediction
        logit = logits[-1]
        self.top_values, self.top_indexs = tf.nn.top_k(logit, k = self.beam_size, sorted=True)

        #load variable
        saver = tf.train.Saver()
        saver.restore(self.sess, self.save_path+"/model.ckpt")
        print("\nModel restored.")









#----------------------------prediciton helper function-----------------------------
    @staticmethod
    def build_input(sequence):
        dec_inp = np.zeros((1,len(sequence)))
        dec_inp[0][:] = sequence
        return dec_inp.T

    def print_sentence(self,index_list):
        return " ".join([self.reader.id_dict[index] for index in index_list])



    def beam_predict(self,enc_inp):
        
        sequnece = [2]
        
        dec_inp = self.build_input(sequnece)
        
        candidates = []
        options = []
        
        feed_dict = {self.enc_inputs: enc_inp, self.dec_inputs:dec_inp}
        values, indexs, state = self.sess.run([self.top_values, self.top_indexs, self.dec_states], feed_dict)

        for i in range(len(values)):
            candidates.append([values[i], [indexs[i]]])
            #print(values[i]);print(indexs[i])
            

        best_sequence = None
        highest_score = -sys.maxsize - 1


        while True:

            #print candidates
            for i in range(len(candidates)):

                sequence = candidates[i][1]
                score = candidates[i][0]

                # if sequence end, evaluate
                if sequence[-1] == 3 or len(sequence) >= self.max_sequence_len:
                    if score > highest_score:
                        highest_score = score
                        best_sequence = sequence
                    continue

                # if not, continue searching
                dec_inp = self.build_input(sequence)

                feed_dict = {self.enc_states: state, self.dec_inputs:dec_inp}
                values, indexs = self.sess.run([self.top_values, self.top_indexs], feed_dict)

                for j in range(len(values)):
                    new_sequence = list(sequence)
                    new_sequence.append(indexs[j])
                    options.append([score+values[j], new_sequence])

            # sort all options and keep top k 
            options.sort(reverse = True)
            candidates = []

            for i in range(min(len(options), self.top_k)):
                if options[i][0] > highest_score:
                    candidates.append(options[i])

            options = []
            if len(candidates) == 0:
                break

        return best_sequence[:-1]



    def translate(self,token_list):
        enc = []
        for token in token_list:
            if token in self.reader.token_count_dict:
                enc.append(self.reader.token_count_dict[token])
            else:
                enc.append(self.reader.token_count_dict['[unk]'])
        #dec will be append with 2 inside the model
        return enc


#---------------------------prediction loop---------------------------

    def decode_line(self,line):
        try:
            token_list = re.findall(self.expression, line.lower())
            token_list = [self.english.stem(w) for w in token_list]
            sequence = self.translate(token_list)
            enc_inp = self.build_input(sequence[::-1])
            response = self.beam_predict(enc_inp)
            return self.print_sentence(response)
        except KeyboardInterrupt:
            self.sess.close()
