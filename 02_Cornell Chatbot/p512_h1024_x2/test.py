import tensorflow as tf
import numpy as np
import s2s_reader
import sys
import re
from nltk.stem.snowball import EnglishStemmer


root_folder="/media/skyap/Data/Cornell Chatbot"


#-----------------------------------parameters------------------------------------------

#interactive mode allow user to talk to the model directly, if set to false, it will test on the training data instead
iteracitve = True
#regular expression for parsing user input
expression = r"[a-zA-Z0-9']+"
english=EnglishStemmer()

#batch size for testing
batch_size = 1

#data params
#bucket_option = [i for i in xrange(1, 20+1)]
bucket_option = [5,10,15]

reader = s2s_reader.reader("",batch_size,bucket_option)
vocab_size = len(reader.token_count_dict)

# if load_model = true, then we need to define the same parameter in the saved_model inorder to load it 
hidden_size = 1024
projection_size = 512
embedding_size = 300
num_layers = 2
output_size = projection_size

keep_prob=0.95

#model name & save path
# model_name="p"+str(projection_size)+"_h"+str(hidden_size)+"_x"+str(num_layers)
save_path= root_folder+"/model/"+"p512_h1024_x2_20171210-072215"

#prediction params
beam_size = 10
top_k = 10
max_sequence_len = 20





#---------------------------------model definition------------------------------------------
# tf.set_random_seed(1)
tf.reset_default_graph()
sess = tf.InteractiveSession()

#placeholder
enc_inputs = tf.placeholder(tf.int32, shape=(None, batch_size), name="enc_inputs")
targets = tf.placeholder(tf.int32, shape=(None, batch_size), name="targets")
dec_inputs = tf.placeholder(tf.int32, shape=(None, batch_size), name="dec_inputs")

#input embedding layers
emb_weights = tf.Variable(tf.random_normal([vocab_size, embedding_size]), name="emb_weights")
enc_inputs_emb = tf.nn.embedding_lookup(emb_weights, enc_inputs, name="enc_inputs_emb")
dec_inputs_emb = tf.nn.embedding_lookup(emb_weights, dec_inputs, name="dec_inputs_emb")

#cell definiton
def getLSTM():
    cell_list=[]
    for i in range(num_layers):
        single_cell=tf.contrib.rnn.LSTMCell(
                                        num_units=hidden_size,
                                        num_proj=projection_size,
                                        state_is_tuple=True
                                        )
        # if i <num_layers-1 or num_layers ==1:
        #     single_cell=tf.contrib.rnn.DropoutWrapper(cell=single_cell,
        #                                      output_keep_prob=keep_prob)
        cell_list.append(single_cell)
    return tf.contrib.rnn.MultiRNNCell(cells=cell_list,state_is_tuple=True)

#encoder & decoder defintion
_, enc_states = tf.nn.dynamic_rnn(cell = getLSTM(), 
    inputs = enc_inputs_emb, 
    dtype = tf.float32, 
    time_major = True, 
    scope="encoder")

dec_outputs, dec_states = tf.nn.dynamic_rnn(cell = getLSTM(), 
    inputs = dec_inputs_emb, 
    initial_state = enc_states, 
    dtype = tf.float32, 
    time_major = True, 
    scope="decoder")

#output layers
project_w = tf.Variable(tf.truncated_normal([output_size, embedding_size], stddev=0.1), name="project_w")
project_b = tf.Variable(tf.constant(0.1, shape=[embedding_size]), name="project_b")
softmax_w = tf.Variable(tf.truncated_normal([embedding_size, vocab_size], stddev=0.1), name="softmax_w")
softmax_b = tf.Variable(tf.constant(0.1, shape=[vocab_size]), name="softmax_b")

dec_outputs = tf.reshape(dec_outputs, [-1, output_size])
dec_proj = tf.matmul(dec_outputs, project_w) + project_b
logits = tf.nn.log_softmax(tf.matmul(dec_proj, softmax_w) + softmax_b)

#prediction
logit = logits[-1]
top_values, top_indexs = tf.nn.top_k(logit, k = beam_size, sorted=True)

#load variable
saver = tf.train.Saver()
saver.restore(sess, save_path+"/model.ckpt")
print("\nModel restored.")









#----------------------------prediciton helper function-----------------------------

def build_input(sequence):
    dec_inp = np.zeros((1,len(sequence)))
    dec_inp[0][:] = sequence
    return dec_inp.T

def print_sentence(index_list):
    for index in index_list:
        sys.stdout.write(reader.id_dict[index])
        sys.stdout.write(' ')
    sys.stdout.write('\n')




def beam_predict(enc_inp):
    
    sequnece = [2]
    
    dec_inp = build_input(sequnece)
    
    candidates = []
    options = []
    
    feed_dict = {enc_inputs: enc_inp, dec_inputs:dec_inp}
    values, indexs, state = sess.run([top_values, top_indexs, dec_states], feed_dict)

    for i in range(len(values)):
        candidates.append([values[i], [indexs[i]]])
        #print(values[i]);print(indexs[i])
        

    best_sequence = None
    highest_score = -sys.maxsize - 1


    while True:

        #print candidates
        for i in range(len(candidates)):

            sequence = candidates[i][1]
            score = candidates[i][0]

            # if sequence end, evaluate
            if sequence[-1] == 3 or len(sequence) >= max_sequence_len:
                if score > highest_score:
                    highest_score = score
                    best_sequence = sequence
                continue

            # if not, continue searching
            dec_inp = build_input(sequence)

            feed_dict = {enc_states: state, dec_inputs:dec_inp}
            values, indexs = sess.run([top_values, top_indexs], feed_dict)

            for j in range(len(values)):
                new_sequence = list(sequence)
                new_sequence.append(indexs[j])
                options.append([score+values[j], new_sequence])

        # sort all options and keep top k 
        options.sort(reverse = True)
        candidates = []

        for i in range(min(len(options), top_k)):
            if options[i][0] > highest_score:
                candidates.append(options[i])

        options = []
        if len(candidates) == 0:
            break

    return best_sequence[:-1]



def translate(token_list):
    enc = []
    for token in token_list:
        if token in reader.token_count_dict:
            enc.append(reader.token_count_dict[token])
        else:
            enc.append(reader.token_count_dict['[unk]'])
    #dec will be append with 2 inside the model
    return enc













#---------------------------prediction loop---------------------------

if iteracitve:

    print("\n--------------------------")
    print("--Interactive mode is on--")
    print("--------------------------\n")
	
    while True:
        try:
            #line = sys.stdin.readline()
            line = input(">")
        except KeyboardInterrupt:
            print("\nsession close")
            break

        token_list = re.findall(expression, line.lower())
        token_list = [english.stem(w) for w in token_list]
        print("-------------")

        sequence = translate(token_list)
        enc_inp = build_input(sequence[::-1])
        response = beam_predict(enc_inp)
        sys.stdout.write('src: ')
        print_sentence(sequence)
        sys.stdout.write('-->: ')
        print_sentence(response)
        print(' ')

else:

    while True:
        try:
            data,index = reader.next_batch()
            enc_inp, dec_inp, dec_tar = s2s_reader.data_processing(data,index)
            response = beam_predict(enc_inp)

            sys.stdout.write('src: ')
            print_sentence(data[0][0])
            sys.stdout.write('tar: ')
            print_sentence(data[0][1])
            sys.stdout.write('-->: ')
            print_sentence(response)
            print(' ')
        except KeyboardInterrupt:
            print("\nsession close")
            break



sess.close()
