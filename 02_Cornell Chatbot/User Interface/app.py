from flask import Flask, render_template, request
from flask import jsonify
import sys
import os.path
import netifaces
# print(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir,"","p512_h1024_x2")))
# sys.exit()
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), os.path.pardir,"","p512_h1024_x2")))
from execute import execute


#############
app = Flask(__name__,static_url_path="/static")
# Routing
#
@app.route('/message', methods=['POST'])
def reply():
    line=request.form['msg']
    respond=ex.decode_line(line)
    return jsonify( { 'text': respond } )
	#return jsonify({'text':'hello'})
@app.route("/")
def index():
    return render_template("index.html")
#############

'''
Init seq2seq model

    1. Call main from execute.py
    2. Create decode_line function that takes message as input
'''
#_________________________________________________________________


ex = execute()
host=netifaces.ifaddresses('wlp2s0')[netifaces.AF_INET][0]['addr']

# start app
app.run(host=host,port=5000,debug=False)
