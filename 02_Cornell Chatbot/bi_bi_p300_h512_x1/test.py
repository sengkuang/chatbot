import tensorflow as tf
import numpy as np
import s2s_reader
import sys
import re
from nltk.stem.snowball import EnglishStemmer


root_folder="/media/skyap/Data/Cornell Chatbot"


#-----------------------------------parameters------------------------------------------

#interactive mode allow user to talk to the model directly, if set to false, it will test on the training data instead
iteracitve = True
#regular expression for parsing user input
expression = r"[a-zA-Z0-9']+"
english=EnglishStemmer()

#batch size for testing
batch_size = 1

#data params
#bucket_option = [i for i in xrange(1, 20+1)]
bucket_option = [5,10,15]

reader = s2s_reader.reader("",batch_size,bucket_option)
vocab_size = len(reader.token_count_dict)

# if load_model = true, then we need to define the same parameter in the saved_model inorder to load it 
hidden_size = 512
projection_size = 300
embedding_size = 300
num_layers = 1
output_size = projection_size


keep_prob=0.95

#model name & save path
# model_name="p"+str(projection_size)+"_h"+str(hidden_size)+"_x"+str(num_layers)
save_path= root_folder+"/model/"+"bi_bi_p300_h512_x1_20171209-094626"

#prediction params
beam_size = 10
top_k = 10
max_sequence_len = 20





#---------------------------------model definition------------------------------------------
tf.reset_default_graph()
sess=tf.InteractiveSession()

enc_inputs=tf.placeholder(tf.int32,shape=(None,batch_size),name="enc_inputs")
dec_inputs=tf.placeholder(tf.int32,shape=(None,batch_size),name="dec_inputs")

emb_weights=tf.Variable(tf.random_normal([vocab_size,embedding_size]),name="emb_weights")
enc_inputs_emb=tf.nn.embedding_lookup(emb_weights,enc_inputs,name="enc_inputs_emb")
dec_inputs_emb=tf.nn.embedding_lookup(emb_weights,dec_inputs,name="dec_inputs_emb")


def getLSTM():
    cell_list=[]
    for i in range(num_layers):
        single_cell=tf.contrib.rnn.LSTMCell(
                                        num_units=hidden_size,
                                        num_proj=projection_size,
                                        state_is_tuple=True
                                        )
        # if i <num_layers-1 or num_layers ==1:
        #     single_cell=tf.contrib.rnn.DropoutWrapper(cell=single_cell,
        #                                      output_keep_prob=keep_prob)
        cell_list.append(single_cell)
    return tf.contrib.rnn.MultiRNNCell(cells=cell_list,state_is_tuple=True)



_,(enc_states_fw,enc_states_bw)=tf.nn.bidirectional_dynamic_rnn(cell_fw=getLSTM(),
                                cell_bw=getLSTM(),
                               inputs=enc_inputs_emb,
                               dtype=tf.float32,
                               time_major=True,
                               scope='encoder')

(dec_outputs_fw,dec_outputs_bw),(dec_states_fw,dec_states_bw)=tf.nn.bidirectional_dynamic_rnn(cell_fw=getLSTM(),
    cell_bw=getLSTM(),
    inputs=dec_inputs_emb,
    initial_state_fw=enc_states_fw,
    initial_state_bw=enc_states_bw,
    dtype=tf.float32,
    time_major=True,
    scope='decoder')



project_w_fw=tf.Variable(tf.truncated_normal(shape=[output_size,embedding_size],
                                          stddev=truncated_std),name="project_w_fw")
project_b_fw=tf.Variable(tf.constant(shape=[embedding_size],value=0.1),name="project_b_fw")

project_w_bw=tf.Variable(tf.truncated_normal(shape=[output_size,embedding_size],
                                          stddev=truncated_std),name="project_w_bw")
project_b_bw=tf.Variable(tf.constant(shape=[embedding_size],value=0.1),name="project_b_bw")


softmax_w_fw=tf.Variable(tf.truncated_normal(shape=[embedding_size,vocab_size],
                                          stddev=truncated_std),name="softmax_w_fw")
softmax_w_bw=tf.Variable(tf.truncated_normal(shape=[embedding_size,vocab_size],
                                          stddev=truncated_std),name="softmax_w_bw")

softmax_b=tf.Variable(tf.constant(shape=[vocab_size],value=0.1),name="softmax_b")

dec_outputs_fw=tf.reshape(dec_outputs_fw,[-1,output_size])
dec_proj_fw=tf.matmul(dec_outputs_fw,project_w_fw)+project_b_fw

dec_outputs_bw=tf.reshape(dec_outputs_bw,[-1,output_size])
dec_proj_bw=tf.matmul(dec_outputs_bw,project_w_bw)+project_b_bw

logits=tf.nn.log_softmax(tf.add(tf.add(tf.matmul(dec_proj_fw,softmax_w_fw),
    tf.matmul(dec_proj_bw,softmax_w_bw)),softmax_b))

#---------------------------------model definition------------------------------------------

#prediction
logit = logits[-1]
top_values, top_indexs = tf.nn.top_k(logit, k = beam_size, sorted=True)

#load variable
saver = tf.train.Saver()
saver.restore(sess, save_path+"/model.ckpt")
print("\nModel restored.")









#----------------------------prediciton helper function-----------------------------

def build_input(sequence):
    dec_inp = np.zeros((1,len(sequence)))
    dec_inp[0][:] = sequence
    return dec_inp.T

def print_sentence(index_list):
    for index in index_list:
        sys.stdout.write(reader.id_dict[index])
        sys.stdout.write(' ')
    sys.stdout.write('\n')




def beam_predict(enc_inp):
    
    sequnece = [2]
    
    dec_inp = build_input(sequnece)
    
    candidates = []
    options = []
    
    feed_dict = {enc_inputs: enc_inp, dec_inputs:dec_inp}
    values, indexs, states_fw,states_bw = sess.run([top_values, top_indexs, dec_states_fw,dec_states_bw], feed_dict)

    for i in range(len(values)):
        candidates.append([values[i], [indexs[i]]])
        #print(values[i]);print(indexs[i])
        

    best_sequence = None
    highest_score = -sys.maxsize - 1


    while True:

        #print candidates
        for i in range(len(candidates)):

            sequence = candidates[i][1]
            score = candidates[i][0]

            # if sequence end, evaluate
            if sequence[-1] == 3 or len(sequence) >= max_sequence_len:
                if score > highest_score:
                    highest_score = score
                    best_sequence = sequence
                continue

            # if not, continue searching
            dec_inp = build_input(sequence)

            feed_dict = {enc_states_fw:states_fw,enc_states_bw:states_bw, dec_inputs:dec_inp}
            values, indexs = sess.run([top_values, top_indexs], feed_dict)

            for j in range(len(values)):
                new_sequence = list(sequence)
                new_sequence.append(indexs[j])
                options.append([score+values[j], new_sequence])

        # sort all options and keep top k 
        options.sort(reverse = True)
        candidates = []

        for i in range(min(len(options), top_k)):
            if options[i][0] > highest_score:
                candidates.append(options[i])

        options = []
        if len(candidates) == 0:
            break

    return best_sequence[:-1]



def translate(token_list):
    enc = []
    for token in token_list:
        if token in reader.token_count_dict:
            enc.append(reader.token_count_dict[token])
        else:
            enc.append(reader.token_count_dict['[unk]'])
    #dec will be append with 2 inside the model
    return enc













#---------------------------prediction loop---------------------------

if iteracitve:

    print("\n--------------------------")
    print("--Interactive mode is on--")
    print("--------------------------\n")
	
    while True:
        try:
            #line = sys.stdin.readline()
            line = input(">")
        except KeyboardInterrupt:
            print("\nsession close")
            break

        token_list = re.findall(expression, line.lower())
        token_list = [english.stem(w) for w in token_list]
        print("-------------")

        sequence = translate(token_list)
        enc_inp = build_input(sequence[::-1])
        response = beam_predict(enc_inp)
        sys.stdout.write('src: ')
        print_sentence(sequence)
        sys.stdout.write('-->: ')
        print_sentence(response)
        print(' ')

else:

    while True:
        try:
            data,index = reader.next_batch()
            enc_inp, dec_inp, dec_tar = s2s_reader.data_processing(data,index)
            response = beam_predict(enc_inp)

            sys.stdout.write('src: ')
            print_sentence(data[0][0])
            sys.stdout.write('tar: ')
            print_sentence(data[0][1])
            sys.stdout.write('-->: ')
            print_sentence(response)
            print(' ')
        except KeyboardInterrupt:
            print("\nsession close")
            break



sess.close()
